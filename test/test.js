const { assert } = require('chai');
const { newUser } = require('../index.js')

// describe gives structure to your test suite
describe('Test newUser Object', () => {
    // unit test
    it('Assert newUser type is object', ()=>{
        assert.equal(typeof(newUser), 'object')
    });
    it('Assert newUser.email type is string', ()=>{
        assert.equal(typeof(newUser.email), 'string')
    });
    it('Assert newUser.password type is string', ()=>{
        assert.equal(typeof(newUser.password), 'string')
    });

    // Mini-Activity:
    // Create a test that asserts that newUser.email IS NOT equal to undefined

    // .equal = testing for equality ==

    it('Assert newUser.email type IS NOT equal to undefined', ()=>{
        assert.notEqual(typeof(newUser.email), undefined)
    });

    // .notEqual = testing for inequality !=

    it('Assert that the number of characters in newUser.password is at least 16', () => {
        // .isAtLeast = testing for greater than or equal to >=
        assert.isAtLeast(newUser.password.length, 16)
    });

    // Activity Instructions:
    /* 1. Assert that the newUser firstName type is a string
    2. Assert that the newUser lastName type is a string
    3. Assert that the newUser firstName is not undefined
    4. Assert that the newUser lastName is not undefined
    5. Assert that the newUser age is at least 18
    6. Assert that the newUser age type is a number
    7. Assert that newUser contact number is a string
    8. Assert that newUser contact number is at least 11 characters
    9. Assert that newUser batch number type is a number
    10. Assert that newUser batch number is not undefined */
    it('Assert that the newUser firstName type is a string', ()=>{
        assert.equal(typeof(newUser.firstName), 'string')
    });
    it('Assert that the newUser lastName type is a string', ()=>{
        assert.equal(typeof(newUser.lastName), 'string')
    });
    it('Assert that the newUser firstName is not undefined', ()=>{
        assert.notEqual(typeof(newUser.firstName), undefined)
    });
    it('Assert that the newUser lastName is not undefined', ()=>{
        assert.notEqual(typeof(newUser.lastName), undefined)
    });
    it('Assert that the newUser age is at least 18', () => {
        assert.isAtLeast(newUser.age, 18)
    });
    it('Assert that the newUser age type is a number', ()=>{
        assert.equal(typeof(newUser.age), 'number')
    });
    it('Assert that newUser contact number is a string', ()=>{
        assert.equal(typeof(newUser.contactNumber), 'string')
    });
    it('Assert that newUser contact number is at least 11 characters', () => {
        assert.isAtLeast(newUser.contactNumber.length, 11)
    });
    it('Assert that newUser batch number type is a number', ()=>{
        assert.equal(typeof(newUser.batchNumber), 'number')
    });
    it('Assert that newUser batch number is not undefined', ()=>{
        assert.notEqual(typeof(newUser.batchNumber), undefined)
    });
});